
/* Written by Albert Ong
 * 
 * A program designed to write a string to a separate text file. 
 */

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class StringtoText {
	
    public static void main(String[] args) throws IOException {


        BufferedWriter writer = 
			new BufferedWriter(new FileWriter("test.txt"));
		
		String test_text = "This is a string. \n" 
						 + "This is another string";
		
		System.out.println(test_text);
		
		writer.write(test_text);
        writer.close();
    }
}

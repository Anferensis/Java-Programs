
/* Written by Albert Ong
 * 
 * COMSC 76
 * Excercise 18.12
 */

import java.util.Scanner;


public class Excercise_18_12 {
	
	// A method that returns a reversed string.
    public static void reverseDisplay(String value) {
		
		// Uses the helper method. 
		reverseDisplay(value, value.length());
	}
	
	
	// The helper method.
	public static void reverseDisplay(String value, int high) {
		
		// The base case of the function, where the length of the
		// string is equal to 0. 
		if (high == 0) {
			
			// Exits the function.
			return;
		}
		
		// When the length of the string is not equal to 0...
		else {
			
			// Prints out the last character of the string.
			char last_char = value.charAt(high - 1);
			System.out.print(last_char);
			
			// Deletes the last character of the string
			String reduced_value = value.substring(0, high - 1);
			
			// Runs the function again using the reduced string.
			reverseDisplay(reduced_value, high - 1);
			
			// This block will keep running until the length of the
			// string is equal to zero, then it will exit through
			// the base case. 
		}
	}
	
	
	public static void main(String args[]) {
		
		// Creates a Scanner object. 
		Scanner scanner = new Scanner(System.in);
		
		// Prompts the user to enter a string.
		System.out.println("Enter a string: ");
		String input = scanner.nextLine();
		
		// Displays the reversed string input.
		System.out.println("\nReversed input: ");
		reverseDisplay(input);
	}
}


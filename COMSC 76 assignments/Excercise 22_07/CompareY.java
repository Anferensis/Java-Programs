
/* Written by Albert Ong
 *
 * COMSC 76
 * Excercise 22.07
 */

import java.util.Comparator;


public class CompareY implements Comparator<Point> {
	
	@Override
	public int compare(Point point1, Point point2) {
		
		// Retrives the x and y values for point1 and point2.
		double point1_x = point1.getX();
		double point1_y = point1.getY();
		
		double point2_x = point2.getX();
		double point2_y = point2.getY();
		
		// Creates a value that will become the final returned value.
		int return_value = 0;
		
		// First compares the y values of each point. 
		if (point1_y > point2_y) {
			return_value = 1;
		}
		else if (point1_y < point2_y) {
			return_value = -1;
		}
		
		// If the y values of each point are equal...
		else {
			
			// Compares the x values of each point.
			if (point1_x > point2_x) {
				return_value = 1;
			}
			else if (point1_x < point2_x) {
				return_value = -1;
			}
		}
		// If both the x and y values of each point are equal, 
		// then return_value will equal 0.
		
		// Returns the final value. 
		return return_value;
	}
}

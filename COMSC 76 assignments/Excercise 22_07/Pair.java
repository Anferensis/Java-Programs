
/* Written by Albert Ong
 *
 * COMSC 76
 * Excercise 22.07
 */

class Pair {
	
	// Two private variables that represent two Points.
	private static Point p1;
	private static Point p2;
	
	// Constructor for the Pair class
	public Pair(Point p1_input, Point p2_input) {
		
		// Assigns each point to the inputted Points.
		p1 = p1_input;
		p2 = p2_input;
	}
	
	// Setter methods of the Pair class.
	public static void setP1(Point p) {
		p1 = p;
	}
	public static void setP2(Point p) {
		p2 = p;
	}
	
	// Getter methods of the Pair class. 
	public static Point getP1() {
		return p1;
	}
	public static Point getP2() {
		return p2;
	}
	
	// A method that calculates the distance between the two points
	// of a Pair. 
	public static double getDistance() {
		
		// Retrieves the x and y values of p1 and p2.
		double x1 = p1.getX();
		double y1 = p1.getY();
		
		double x2 = p2.getX();
		double y2 = p2.getY();
		
		// Uses the Pythagorean theorem to calculate the distance between
		// the two points. 
		double distance = 
			Math.sqrt(Math.pow(x1 - x2, 2) + 
					  Math.pow(y1 - y2, 2));
		
		// Returns the distance. 
		return distance;
	}
	
	// The toString method of the Pair class. 
	public String toString() {
		
		return "Pair: \n" + 
				p1.toString() + "\n" +
				p2.toString();
	}
}




/* Written by Albert Ong
 *
 * COMSC 76
 * Excercise 22.07
 */

import java.lang.Math;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;


public class Excercise_22_07 {
	
	/** Return the distance of the closest pair of points */
	public static Pair getClosestPair(double[][] points) {
		
		// An array of points that will store assembled Point objects. 
		Point[] assembled_points = new Point[points.length];
		
		// Uses a for loop to assemble all the Points. 
		for (int index = 0; index < points.length; index++) {
			
			// Retrieves the data from the array of array of doubles. 
			double[] data = points[index];
			
			// Assembles a new Point from the retrieve data. 
			Point new_point = new Point(data[0], data[1]);
			
			// Assigns the new Point to the array of Points.
			assembled_points[index] = new_point;
		}
		
		// Uses and overridden method to retrive the closest pair.
		return getClosestPair(assembled_points);
	}


	/** Return the distance of the closest pair of points */
	public static Pair getClosestPair(Point[] points) {
		
		// Stores the array of points sorted by their x values.
		Arrays.sort(points);
		Point[] pointsOrderedOnX = points.clone();
		
		// Stores the array of points sorted by their y values.
		Arrays.sort(points, new CompareY());
		Point[] pointsOrderedOnY = points.clone();
		
		// Uses an overridden method to retrive the closest pair. 
		return distance(pointsOrderedOnX, 0, points.length - 1, pointsOrderedOnY);
	}


	 /** Return the distance of the closest pair of points
	 * in pointsOrderedOnX[low..high]. This is a recursive
	 * method. pointsOrderedOnX and pointsOrderedOnY are
	 * not changed in the subsequent recursive calls.
	 */
	public static Pair distance(
		Point[] pointsOrderedOnX, int low, int high, Point[] pointsOrderedOnY) {
		
		// Determines two base case conditions. 
		boolean examineOnePoint = low == high;
		boolean examineTwoPoints = low + 1 == high;
		
		// A base case when low and high are equal
		// (both indices refer to the same point)
		if (examineOnePoint) {
		
			// Returns a null value.
			return null;
		} 
		
		// A base case where low and high refer to two Points
		// that are next to each other. 
		else if (examineTwoPoints) {
			
			// Returns the two Points as a Pair.	
			Pair pair = new Pair(pointsOrderedOnX[0], pointsOrderedOnX[1]);			
			return pair;
		}
		
		// Calculates the middle index. 
		int mid = (low + high) / 2;
		
		// Uses a recursive call to find the pair of Points with the smallest
		// distance in both the lower and upper halves of the array.
		// These are assigned to pair1 and pair2 respectively. 
		Pair pair1 = distance(pointsOrderedOnX, low, mid, pointsOrderedOnY);
		Pair pair2 = distance(pointsOrderedOnX, mid + 1, high, pointsOrderedOnY);
		
		// A double that will represent the smallest distance between
		// any two Points in the array. 
		double distance = 0;
		
		// A pair that will store the Points with the smallest distance
		// between each other.  
		Pair pair = null;
		
		// Checks if pair1, pair2, or both pair1 and pair2 are null. 
		boolean bothPairsAreNull = pair1 == null && pair2 == null;
		boolean pair1IsNull = pair1 == null;
		boolean pair2IsNull = pair2 == null;
		
		// If both pair1 and pair2 are null values. 
		if (bothPairsAreNull) {
			
			// Distance is set to the largest possible value. 
			distance = Double.MAX_VALUE;
		} 
		
		// If only pair1 returns a null value.
		else if (pair1IsNull) {
			
			// distance and pair are assigned by the values of pair2. 
			distance = pair2.getDistance();
			pair = pair2;
		} 
		
		// If only pair2 returns a null value. 
		else if (pair2IsNull) {
			
			// distance and pair are assigned by the values of pair1. 
			distance = pair1.getDistance();
			pair = pair1;
		} 
		
		// Otherwise...
		else {
			
			// Determines the smallest distance between either pair1 or pair2. 
			distance = Math.min(pair1.getDistance(), pair2.getDistance());
			
			// The Pair with the smallest distance becomes the value of pair.
			if (pair1.getDistance() <= pair2.getDistance()) {
				pair = pair1;
			}
			else {
				pair = pair2;
			}
		}
		
		// Creates two ArrayLists that will represent the left and right strip.
		ArrayList<Point> stripL = new ArrayList<Point>();
		ArrayList<Point> stripR = new ArrayList<Point>();
		
		// Retrieves the x value of the middle Point. 
		double midX = pointsOrderedOnX[mid].getX();
		
		// Uses a for loop to assemble stripL and stripR.
		for (Point point : pointsOrderedOnY) {
			
			// Determines if a Point lies within stripL.
			boolean instripL = 
				(point.getX() <= midX) &&
				(point.getX() >= midX - distance);
			
			// If so, adds the Point to stripL. 
			if (instripL) {			
				stripL.add(point);
			} 
			// Otherwise adds the Point to stripR. 
			else {
				stripR.add(point);
			}
		}
		
		// A variable that will become the shortest distance amongst all
		// the Points in the array. If left unchanged, if will simply
		// be the shortest distance between either the lower or upper
		// half of the array. 
		double distance3 = distance;
		
		// A variable that will count the index in stripR.
		int stripRIndex = 0;
		
		// Uses a for loop to examine the Points in stripL and stripR. 
		for (Point stripLPoint : stripL) {
			
			// Ignores the points in stripR that are below the y value of
			// stripLPoint minus distance. 
			while (stripRIndex < stripR.size() &&
				   stripR.get(stripRIndex).getY() <= stripLPoint.getY() - distance) {
				
				stripRIndex++;
			}
			
			// Examines the Point in stripL and stripR. 
			while (stripRIndex < stripR.size() && 
				   stripR.get(stripRIndex).getY() <= stripLPoint.getY() + distance) {
				
				// Finds the distance between the Point on stripL and stripR. 
				double testDistance = distance(stripLPoint, stripR.get(stripRIndex));
				
				// Tests if the Point in stripL and stripR are closer than
				// the shortest distance in either the lower or upper
				// half of the array. 
				boolean closestPair = testDistance < distance3;
				
				// If so...
				if (closestPair) {
					
					// Redefines the closest distance. 
					distance3 = testDistance;
					
					// Redefines the Pair with the lowest distance. 
					pair.setP1(stripLPoint);
					pair.setP2(stripR.get(stripRIndex));
					}
				
				// Increases the index count. 
				stripRIndex++;
			}
		}
		// By the end of this for loop, distance3 will represent the shortest
		// distance between any two Points in the arrya and pair will
		// represent the Pair of these Points. 
		
		// Returns the Pair of Points with the smallest distance between them. 
		return pair;
	}
	
	
	/** Compute the distance between two points p1 and p2 */
	public static double distance(Point p1, Point p2) {
		
		// Retrives the x and y values for p1 and p2.
		double x1 = p1.getX();
		double y1 = p1.getY();
		
		double x2 = p2.getX();
		double y2 = p2.getY();
		
		// Uses an overridden method to calculate the distance. 
		return distance(x1, y1, x2, y2);
	}
	
	
	/** Compute the distance between points (x1, y1) and (x2, y2) */
	public static double distance(double x1, double y1, double x2, double y2) {
		
		// Calculates the distance between two Points using
		// the Pythagorean theorem. 
		double distance = 
			Math.sqrt(Math.pow(x1 - x2, 2) + 
					 Math.pow(y1 - y2, 2));
		
		// Returns the distance. 						
		return distance;
	}
	
	
	public static void main(String args[]) {
		
		// Creates an array of Points that will store 100 Point objects. 
		Point[] points = new Point[100];
		
		// Uses a for loop to add 100 Points. 
		for (int num = 0; num < 100; num++) {
			
			// Creates a Point with a random x and y value.
			Point random_point = 
				new Point(100 * new Random().nextDouble(),
						  100 * new Random().nextDouble());
			
			// Adds the Point to the array.		  
			points[num] = random_point;
		}
		
		// Calculates the lowest and highest index of the array of Points.
		int low = 0;
		int high = points.length - 1;
		
		// Sorts the Points by x value.
		Arrays.sort(points);
		Point[] pointsOrderedOnX = points.clone();
		
		// Sorts the Points by y value.
		Arrays.sort(points, new CompareY());
		Point[] pointsOrderedOnY = points.clone();
		
		// Finds the closest pair within the array of Points.
		Pair closest_pair =	
			distance(pointsOrderedOnX, low, high, pointsOrderedOnY);
		
		// Prints out the closest Pair.
		System.out.println(closest_pair);
		
		// Prints out the distance between the pairs.
		System.out.println("Distance: " + closest_pair.getDistance());
	}
}



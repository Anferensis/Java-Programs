
/* Written by Albert Ong
 * 
 * COMSC 76
 * Excercise 13.01
 */

// package chapter13;

public class Triangle extends GeometricObject {
	
	// Creates three doubles which represent the three sides
	// of a Triangle object. 
	private double side1;
	private double side2;
	private double side3;
	
	// The constructor of the Triangle object. 
	public Triangle(double side1_input, 
					double side2_input, 
					double side3_input, 
					String color, 
					boolean filled) {
		
		// Assigns all three sides as well as the color of the object
		// and whether or not the object is filled. 
		side1 = side1_input;
		side2 = side2_input;
		side3 = side3_input;
		setColor(color);
		setFilled(filled);
	}
	
	@Override
	public double getArea() {
		
		// Uses Heron's Formula to calculate the area of the triangle.
		double s = (side1 + side2 + side3) / 2;
		double area = Math.sqrt(s * (s - side1) * (s - side2) * (s - side3));
		
		// Rounds the area to two decimal places. 
		double rounded_area = Double.parseDouble(String.format("%,.2f", area));
		
		// Returns the rounded area. 
		return rounded_area;
	}
	
	@Override
	public double getPerimeter() {
		
		// Returns the perimeter of the triangle, which is just the 
		// sum of all three sides. 
		return side1 + side2 + side3;
	}
	
	@Override 
	public String toString() {
		
		// Converts the perimeter, filled value, and area to strings. 
		String perimeter = String.valueOf(getPerimeter());
		String filled = String.valueOf(isFilled());
		String area = String.valueOf(getArea());
		
		// Assembles a string that displays the information of a Triangle. 
		String string = 
			"Triangle \n" 
		  + "Area: " + area +"\n"
		  + "Perimeter: " + perimeter + "\n"
		  + "Color: " + getColor() + "\n"
		  + "Filled: " + filled +"\n";
		
		// Returns the final string. 
		return string;
	}
}

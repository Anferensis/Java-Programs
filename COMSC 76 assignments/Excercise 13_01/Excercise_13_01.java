
/* Written by Albert Ong
 * 
 * COMSC 76
 * Excercise 13.01
 */

import java.util.Scanner;

public class Excercise_13_01 {
	
	public static void main(String args[]) {
		
		Scanner scanner = new Scanner(System.in);
		
		// Prompts the user to input every value necessary to construct
		// a Triangle object: three sides, a color, and whether or not
		// shape is filled. 
		System.out.println("Input the length of side #1: ");
		double side1 = scanner.nextDouble();
		
		System.out.println("Input the length of side #2: ");
		double side2 = scanner.nextDouble();
		
		System.out.println("Input the length of side #3: ");
		double side3 = scanner.nextDouble();
		
		System.out.println("Input the color of the triangle: ");
		String color = scanner.next();
		
		System.out.println("Input whether the triangle is filled: ");
		boolean filled = scanner.nextBoolean();
		
		// Constructs a Triangle object using the user's input.
		Triangle triangle = new Triangle(side1, side2, side3, color, filled);
		
		// Prints out the Triangle object.
		System.out.println("\n" + triangle);		
	}
}

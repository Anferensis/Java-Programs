
/* Written by Albert Ong
 *
 * COMSC 76
 * Excercise 21.08
 */

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;


public class Excercise_21_08  {
	
  public static void main(String[] args) throws IOException {
	
	// Prompts the user to enter a file name.
	System.out.println("Enter a file name: ");
	
	// A variable that will store the inputted file name.
	String file_name;
	
	// A variable that will store the file reader.
	BufferedReader reader;
	
	// A while loop that will run indefinitely until a valid file
	// name is inputted. 
	while (true) {
		
		// Retrieves the user's inputted file name.
		file_name = System.console().readLine();
		
		try {
			// Attempts to create a file reader with the given file name.
			reader = new BufferedReader(new FileReader(file_name));
			
			// Exits the while loop if a valid file name is given.
			break;
		}
		
		// Tells the user if they have inputted an invalid file name.
		catch (FileNotFoundException e) {
			System.out.println("Invalid file name");		
		}			
	}
	
	// A variable that will store the text file.
	String text = "";
	
	// Retrieves the first line of the text file.
	String line = reader.readLine();
	
	// While the line is not blank...
	while (line != null) {
		
		// Adds the line to the text variable.
		text += line + "\n";
		
		// Retrieves the next line from the text file.
		line = reader.readLine();
	}
	// By the end of this while loop, the text variable should represent
	// the entire text file.
	
    // Create a TreeMap to hold words as key and count as value
    Map<String, Integer> map = new TreeMap<>();
	
	// Splits the text at all white spaces and punctuation, 
	// thus creating an array of words.
    String[] words = text.split("[\\s+\\p{P}]");
    
    // Uses a for loop to analyze each word.
    for (String word : words) {
		
		// Lowercases the word.
		String key = word.toLowerCase();
		
		// If the map does not contain the key...
		if (!map.containsKey(key)) {
			
			// Adds the key to the map.
			map.put(key, 1);
		}
        
        // Otherwise...
		else {	
			// Adds one to the key value.
			map.put(key, map.get(key) + 1);
			}
		}
	
    // Displays the key and value for each entry.
    System.out.println("\nWord count: ");
    map.forEach((key, value) -> System.out.println(value + "\t" + key));
  }
}

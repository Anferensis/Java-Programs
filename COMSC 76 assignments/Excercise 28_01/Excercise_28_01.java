
/* Written by Albert Ong
 *
 * COMSC 76
 * Excercise 28.01
 */

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class Excercise_28_01{
	
	public static void main(String args[]) throws IOException {
		
		// Asks the user to input a file name. 
		String file_name = System.console().readLine("Input file name: ");
		
		// Creates a graph using the data from the inputted file name. 
		UnweightedGraph graph = readGraphData(file_name);
		
		// Prints out the number of vertices of the graph, the edges, 
		// and whether or not the graph is connected. 
		System.out.println("The number of vertices is " + graph.getSize());
		graph.printEdges();
		isGraphConnected(graph);
	}
	
	
	// A method that contructed a graph given a specified file name. 
	public static UnweightedGraph readGraphData(String file_name) throws IOException {
		
		// A matrix of integers that will represent the graph's data. 
		List<List<Integer>> graphData = new ArrayList<>();
		
		// Creates a BufferedReader that will read the text file. 
		BufferedReader reader = 
			new BufferedReader(new FileReader(file_name));
		
		// Retrieves the first line of the text file. 
		String line = reader.readLine();
		
		// A while loop that will continue until the end of the text file. 
		while (line != null) {
			
			// An ArrayList that will store the numerical data of
			// a line of text. 
			ArrayList<Integer> addLine = new ArrayList<>();
			
			// Splits the line into an array of integer strings. 
			String[] nums = line.split(" ");
			
			// Adds each  integer to the ArrayList. 
			for (String num : nums) {
				addLine.add(Integer.parseInt(num));
			}
			
			// Adds the ArrayList to the graph data and continues to
			// read the next line. 
			graphData.add(addLine);		
			line = reader.readLine();
		}
		// By the end of this while loop, graphData contains the numerical
		// representation of the data of the graph. 
		
		
		// Two lists that will store the vertices and edges of the graph.
		// These will be used to constructed an UnweightedGraph object. 
		List<Integer> vertices = new ArrayList<Integer>();
		List<AbstractGraph.Edge> edges = new ArrayList<AbstractGraph.Edge>();
		
		// Removes the first entry of the graph data, as this only
		// represents the number of vertices. 
		graphData.remove(0);
		
		// Uses a for loop to examine the data of each vertex. 
		for (List<Integer> vertexData : graphData) {
			
			// Retrives the value of the vertex and adds it to 
			// the ArrayList vertices. 
			int vertex = vertexData.get(0);
			vertices.add(vertex);
			
			// Removes the vertex itself for the following for loop. 
			vertexData.remove(0);
			
			// Assembles and adds every edge attached to the vertex
			// into the ArrayList edges. 
			for (Integer connectedVertex : vertexData) {
				edges.add(new AbstractGraph.Edge(vertex, connectedVertex));
			}	
		}
		// By the end of this for loop, vertices and edges will represent
		// all of the vertices and edges of a graph. 
		
		// Returns a contructed UnweightedGraph. 
		return new UnweightedGraph(vertices, edges);
	}
	
	
	// A method that checks if a graph is connected. 
	public static void isGraphConnected(UnweightedGraph graph) {
		
		// Retrieves the number of vertices in the graph. 
		int numberOfGraphVertices = graph.getSize();
		
		// Retrieves the number of vertices in the tree of the graph. 
		AbstractGraph.Tree tree = graph.dfs(0);
		int numberOfTreeVertices = tree.getNumberOfVerticesFound();
		
		// If the number of graph vertices is equal to the number of tree
		// vertices, then the graph is connects. 
		if (numberOfGraphVertices == numberOfTreeVertices) {
			System.out.println("The graph is connected. \n");
		}
		else {
			System.out.println("The graph is not connected. \n");
		}
	}
}


/* Written by Albert Ong
 *
 * COMSC 76
 * Programming Project
 */

import java.util.ArrayList; 


public class ProgrammingProject {
	
	public static void main(String args[]) {
		
		// Creates a MyArrayList and MyHashSet object. 
		MyArrayList<Double> arrayList = new MyArrayList<Double>();
		MyHashSet<Double> hashSet = new MyHashSet<Double>();
		
		// Adds 1,000,000 random doubles between 0 and 999,999
		// to both the MyArrayList and MyHashSet objects. 
		for (int i = 0; i < 1000000; i++) {
			
			double randomDouble = 1000000 * Math.random();
			arrayList.add(randomDouble);
			hashSet.add(randomDouble);
		}
		
		// Creates a list of 1,000,000 random doubles between 0 and 1,999,999. 
		ArrayList<Double> nums = new ArrayList<Double>();
		
		for (int i = 0; i < 1000000; i++) {
			nums.add(2000000 * Math.random());
		}
		
		
		//================= Testing MyHashSet  ========================
		
		// Saves the start time for MyHashSet. 
		long setStartTime = System.currentTimeMillis();
		
		// Tests if each number is in MyHashSet. 
		for (Double num : nums) {
			hashSet.contains(num);
		}
		
		// Calculates and prints out the duration of the operation. 
		long setDuration = System.currentTimeMillis() - setStartTime;
		System.out.println("MyHashSet took " + setDuration + " milliseconds.");
		
		
		//=================  Testing MyArrayList  ======================
		
		// Saves the start time for MyArrayList. 
		long arrayStartTime = System.currentTimeMillis();
		
		// Test if each numer is in MyArrayList. 
		for (Double num : nums) {
			arrayList.contains(num);
		}
		
		// Calculates and prints out the duration of the operation. 
		long arrayDuration = System.currentTimeMillis() - arrayStartTime;
		System.out.println("MyArrayList took " + arrayDuration + " milliseconds.");
	}
}

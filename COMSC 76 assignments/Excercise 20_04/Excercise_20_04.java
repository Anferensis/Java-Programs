
/* Written by Albert Ong
 *
 * COMSC 76
 * Excercise 20.04
 */

import java.util.Arrays;
import java.util.ArrayList;
import java.util.Random;


public class Excercise_20_04 {
	
	public static void main(String args[]) {
		
		// Creates an array of Points that will store 100 Point objects. 
		Point[] points = new Point[100];
		
		// Uses a for loop to add 100 Points. 
		for (int num = 0; num < 100; num++) {
			
			// Creates a Point with a random x and y value.
			Point random_point = 
				new Point(100 * new Random().nextDouble(),
						  100 * new Random().nextDouble());
			
			// Adds the Point to the array.		  
			points[num] = random_point;
		}
		
		// Sorts the Points by x value.
		Arrays.sort(points);
		
		// Prints out the Points. 
		System.out.println("Sorted by x values: ");
		for (Point point : points) {			
			System.out.println(point);
		}
		
		// Sorts the Points by y value.
		Arrays.sort(points, new CompareY());
		
		// Prints out the points. 
		System.out.println("\nSorted by y values: ");
		for (Point point : points) {		
			System.out.println(point);
		}	
	}
}


/* Written by Albert Ong
 *
 * COMSC 76
 * Excercise 20.04
 */

public class Point implements Comparable<Point> {
	
	// Creates two variable to represent the x and y values. 
	private double x;
	private double y;
	
	// The constructor of the Point class.
	public Point(double x_input, double y_input) {
		
		// Assigns the x and y input. 
		x = x_input;
		y = y_input;
	}
		
	// Setter methods of the Point class.
	public void setX(double x_input) {
		x = x_input;
	}
	public void setY(double y_input) {
		y = y_input;
	}
	
	// Getter methods of the Point class.
	public double getX() {
		return x;
	}
	public double getY() {
		return y;
	}
	
	
	@Override 
	public int compareTo(Point other) {
		
		// Retreives the x and y values of the other Point.
		double other_x = other.getX();
		double other_y = other.getY();
		
		int return_value = 0;
		
		// Compares the x values first. 
		if (x > other_x) {
			return_value = 1;
		}
		else if (x < other_x) {
			return_value = -1;
		}
		
		// If the x values are equal...
		else {
			
			// Compares the y values. 
			if (y > other_y) {
				return_value = 1;
			}
			else if (y < other_y) {
				return_value = -1;
			}
		}
		// If both the x and y values of each point are equal, 
		// then return_value will equal 0.
		
		// Returns the final value.
		return return_value;
	}
	
	
	public String toString() {
		
		// Converts the x and y values to strings.
		String x_string = String.format("%.15f", x);
		String y_string = String.format("%.15f", y);
		
		// Adds an extra zero if the length of the strings is less 
		// than 18. This is purely for formatting purposes. 
		if (x_string.length() < 18) {
			x_string += 0;
		}
		if (y_string.length() < 18) {
			y_string += 0;
		}
		
		// Assembles the final output.
		String output = "X: " + x_string + "  Y: " + y_string;
		
		// Returns the output.
		return output;
	}
}

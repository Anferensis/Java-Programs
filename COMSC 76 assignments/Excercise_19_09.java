
/* Written by Albert Ong
 * 
 * COMSC 76
 * Excercise 19.09
 */

import java.util.Arrays;
import java.util.ArrayList;


public class Excercise_19_09 {
	
	// A method that sorts an ArrayList with an arbitrary type E. 
	public static <E extends Comparable<E>> void sort(ArrayList<E> list) {
		
		// Creates two variables, one to store the minimum value
		// and another to store the minimum index. 
		E current_min_item;
		int current_min_index;
		
		// Retrieves the length of the list.
		int list_length = list.size();
		
		// Uses a for loop to analyze each item in the list.
		for (int index1 = 0; index1 < list_length; index1++) {
			
			// Assigns the minimum item and minimum index to the 
			// current index.
			current_min_item = list.get(index1);
			current_min_index = index1;
			
			// Uses a for loop to compare min_item to every other item
			// in the lilst. 
			for(int index2 = index1 + 1; index2 < list_length; index2++) {
				
				// Retrieves the item to compare to.
				E compare_item = list.get(index2);
				
				// Checks if the compare item is less than the current
				// minimum item.
				boolean isLessThanCurrentMin = 
					compare_item.compareTo(current_min_item) < 0;
				
				// If the compare item is less than the current
				// minimum item...
				if (isLessThanCurrentMin) {
					
					// Saves the old minimum value.
					E old_min_item = current_min_item;
					
					// Reassigns the minimum item and mininum index.
					current_min_item = compare_item;
					current_min_index = index2;
					
					// Swaps the old and the new minimum items. 
					list.set(index2, old_min_item);
					list.set(index1, current_min_item);				
				}
			}			
		}
	}
	
	
	// The main function of the program. 
	public static void main(String args[]) {
		
		 // Creates three ArrayLists that store Integers, Doubles, 
		 // and Strings.
		ArrayList<Integer> list1 = 
			new ArrayList<Integer>(Arrays.asList(4, 6, 8, 2, 1, 7));
		
		ArrayList<Double> list2 = 
			new ArrayList<Double>(Arrays.asList(2.5, 3.6, 7.7, 1.2, 0.5));
			
		ArrayList<String> list3 = 
			new ArrayList<String>(Arrays.asList("can", "ban", "ran", "fan", "man"));
		
		
		// Prints out all three ArrayLists both before and after they
		// are sorted. 
		System.out.println("ArrayList of Integers");
		System.out.println("Original list: " + list1);
		sort(list1);
		System.out.println("Sorted list: " + list1 + "\n");
		
		System.out.println("ArrayList of Doubles");
		System.out.println("Original list: " + list2);
		sort(list2);
		System.out.println("Sorted list: " + list2 + "\n");
		
		System.out.println("ArrayList of Strings");
		System.out.println("Original list: " + list3);
		sort(list3);
		System.out.println("Sorted list: " + list3 + "\n");
	}
}


/* Written by Albert Ong
 * 
 * COMSC 76
 * Excercise 23.03
 */

import java.util.Comparator;


public class Excercise_23_03 {
	
	public static <E extends Comparable<E>> void quickSort(E[] list) {
		
		// Calls a helper method that takes the lowest and highest
		// indices as inputs.
		quickSort(list, 0, list.length - 1);			
	}
	
	
	public static <E extends Comparable<E>> 
		void quickSort(E[] list, int low, int high) {
	
		// The base case of the method, where the highest inputted index 
		// must be higher that the lowest inputted index. 
		if (high - low > 0) {
			
			// Uses the last element of the array as a pivot. 
			E pivot = list[high];
			
			// An integer that stores the index above the lower end.
			int low_index = 0;
			
			// Uses a for loop to examine each entry of the array, except
			// the last one - which is the pivot. 
			for (int index = 0; index < high; index++) {
				
				// Retrieves the entry. 
				E entry = list[index];
				
				// Checks if the entry is lower than the pivot. 
				boolean isLower = entry.compareTo(pivot) < 0;
			
				// If so...
				if (isLower) {
					
					// Retrieves the current value above the lower end.
					E swap_value = list[low_index];
					
					// Swaps the value above the lower end with the
					// current entry. 
					list[index] = swap_value;
					list[low_index] = entry;
					
					// Increments the index above the lower end.
					low_index++;
				}
			}
			// By the end of this loop, all elements lower than the pivot
			// are gathered at the front of the array.
			
			// Swaps the pivot with the element in front of the lower elements. 
			list[high] = list[low_index];
			list[low_index] = pivot;
		
			// Recursively sorts the lower and upper portions of the array.
			// This will continue until the portion only contains one element.
			quickSort(list, 0, low_index - 1);
			quickSort(list, low_index + 1, high);
		}
	}
	
	
	public static <E> void quickSort(E[] list, Comparator<? super E> comparator) {
		
		// Calls a helper method that takes the lowest and highest
		// indices as inputs.
		quickSort(list, 0, list.length - 1, comparator);
	}
	
	
	public static <E> void 
		quickSort(E[] list, int low, int high, Comparator<? super E> comparator) {
		
		// The base case of the method, where the highest inputted index 
		// must be higher that the lowest inputted index. 
		if (high - low > 0) {
			
			// Uses the last element of the array as a pivot. 
			E pivot = list[high];
			
			// An integer that stores the index above the lower end.
			int low_index = 0;
			
			// Uses a for loop to examine each entry of the array, except
			// the last one - which is the pivot. 
			for (int index = 0; index < high; index++) {
				
				// Retrieves the entry. 
				E entry = list[index];
				
				// Uses a comparator to check if the entry is lower than
				// the pivot.  
				boolean isLower = comparator.compare(entry, pivot) < 0;
			
				// If so...
				if (isLower) {
					
					// Retrieves the current value above the lower end.
					E swap_value = list[low_index];
					
					// Swaps the value above the lower end with the
					// current entry. 
					list[index] = swap_value;
					list[low_index] = entry;
					
					// Increments the index above the lower end.
					low_index++;
				}
			}
			// By the end of this loop, all elements lower than the pivot
			// are gathered at the front of the array.
			
			// Swaps the pivot with the element in front of the lower elements. 
			list[high] = list[low_index];
			list[low_index] = pivot;
		
			// Recursively sorts the lower and upper portions of the array.
			// This will continue until the portion only contains one element.
			quickSort(list, 0, low_index - 1, comparator);
			quickSort(list, low_index + 1, high, comparator);
		}
	}

	
	// A utility method that prints out all the objects in an array.
	public static void printArray(Object[] list) {
		
		for (int index = 0; index < list.length; index++) {
			System.out.println(list[index]);
		}
		System.out.println();
	}
	
	
	public static void main(String[] args) {
		
		// An array of Integers.
		Integer[] ints = {9, 2, 3, 7, 1, 8, 9, 10, 6, 5};
		
		// An array of Doubles.
		Double[] doubles = {20.0, 1.5, 6.0, 11.6, 3.3, 9.0, 4.5, 5.5, 10.0};
		
		// An array of Strings.
		String[] strings = {"dan", "can", "ban", "fan", "tan", "ran", "man"};
		
		// An array of Characters.
		Character[] chars = {'z', 'v', 'o', 'a', 'c', 'j', 'm'};
		
		// Uses quickSort to sort all of the arrays. 
		quickSort(ints);
		quickSort(doubles);
		quickSort(strings);
		quickSort(chars);
		
		// Prints out all of the arrays. 
		System.out.println("Sorted Integers: ");
		printArray(ints);
		
		System.out.println("Sorted Doubles: ");
		printArray(doubles);
		
		System.out.println("Sorted Strings: ");
		printArray(strings);
		
		System.out.println("Sorted Characters: ");
		printArray(chars);
		
		// An array of GeometricObjects.
		GeometricObject[] geo_objects = 
			{new Circle(5), 
			 new Circle(5.5), 
			 new Rectangle(2.4, 5), 
			 new Circle(0.5), 
			 new Rectangle(4, 65), 
			 new Circle(4.5), 
			 new Rectangle(4.4, 1),
			 new Circle(6.5), 
			 new Rectangle(4, 5)};
		
		// Sorts the array using quickSort. 
		quickSort(geo_objects, new GeometricObjectComparator());
		
		// Prints our the array. 
		System.out.println("Sorted GeometricObjects: ");
		printArray(geo_objects);
	}
}


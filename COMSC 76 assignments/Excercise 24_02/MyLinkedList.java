
/* Written by Albert Ong
 *
 * COMSC 76
 * Excercise 24.02
 */


public class MyLinkedList<E> extends MyAbstractList<E> {
  private Node<E> head, tail;

  /** Create a default list */
  public MyLinkedList() {
  }

  /** Create a list from an array of objects */
  public MyLinkedList(E[] objects) {
    super(objects);
  }

  /** Return the head element in the list */
  public E getFirst() {
    if (size == 0) {
      return null;
    }
    else {
      return head.element;
    }
  }

  /** Return the last element in the list */
  public E getLast() {
    if (size == 0) {
      return null;
    }
    else {
      return tail.element;
    }
  }

  /** Add an element to the beginning of the list */
  public void addFirst(E e) {
    Node<E> newNode = new Node<E>(e); // Create a new node
    newNode.next = head; // link the new node with the head
    head = newNode; // head points to the new node
    size++; // Increase list size

    if (tail == null) // the new node is the only node in list
      tail = head;
  }

  /** Add an element to the end of the list */
  public void addLast(E e) {
    Node<E> newNode = new Node<E>(e); // Create a new for element e

    if (tail == null) {
      head = tail = newNode; // The new node is the only node in list
    }
    else {
      tail.next = newNode; // Link the new with the last node
      tail = tail.next; // tail now points to the last node
    }

    size++; // Increase size
  }


  @Override /** Add a new element at the specified index 
   * in this list. The index of the head element is 0 */
  public void add(int index, E e) {
    if (index == 0) {
      addFirst(e);
    }
    else if (index >= size) {
      addLast(e);
    }
    else {
      Node<E> current = head;
      for (int i = 1; i < index; i++) {
        current = current.next;
      }
      Node<E> temp = current.next;
      current.next = new Node<E>(e);
      (current.next).next = temp;
      size++;
    }
  }

  /** Remove the head node and
   *  return the object that is contained in the removed node. */
  public E removeFirst() {
    if (size == 0) {
      return null;
    }
    else {
      Node<E> temp = head;
      head = head.next;
      size--;
      if (head == null) {
        tail = null;
      }
      return temp.element;
    }
  }

  /** Remove the last node and
   * return the object that is contained in the removed node. */
  public E removeLast() {
    if (size == 0) {
      return null;
    }
    else if (size == 1) {
      Node<E> temp = head;
      head = tail = null;
      size = 0;
      return temp.element;
    }
    else {
      Node<E> current = head;

      for (int i = 0; i < size - 2; i++) {
        current = current.next;
      }

      Node<E> temp = tail;
      tail = current;
      tail.next = null;
      size--;
      return temp.element;
    }
  }

  @Override /** Remove the element at the specified position in this 
   *  list. Return the element that was removed from the list. */
  public E remove(int index) {   
    if (index < 0 || index >= size) {
      return null;
    }
    else if (index == 0) {
      return removeFirst();
    }
    else if (index == size - 1) {
      return removeLast();
    }
    else {
      Node<E> previous = head;

      for (int i = 1; i < index; i++) {
        previous = previous.next;
      }

      Node<E> current = previous.next;
      previous.next = current.next;
      size--;
      return current.element;
    }
  }

  @Override /** Override toString() to return elements in the list */
  public String toString() {
    StringBuilder result = new StringBuilder("[");

    Node<E> current = head;
    for (int i = 0; i < size; i++) {
      result.append(current.element);
      current = current.next;
      if (current != null) {
        result.append(", "); // Separate two elements with a comma
      }
      else {
        result.append("]"); // Insert the closing ] in the string
      }
    }

    return result.toString();
  }

  @Override /** Clear the list */
  public void clear() {
    size = 0;
    head = tail = null;
  }


  @Override /** Return true if this list contains the element e */
  public boolean contains(E e) {
    
    // If the list is blank then the function automatically returns false.
    if (size == 0) {
		return false;
	}
	
	// Otherwise...
	else {
		
		// Starts from the head of the list.
		Node<E> testNode = head;
		
		// Uses a while loop to traverse the list. 
		while (testNode != null) {
			
			// If the element is found, then the function returns true.
			if (testNode.element == e) {
				return true;
			}
			
			// Continues to the next Node.
			testNode = testNode.next;
		}
		
		// If the element was not found in the list, then the
		// function returns false. 
		return false;
	}
  }


  @Override /** Return the element at the specified index */
  public E get(int index) {
    
    // If the index is 0, then the function returns the element of the head.
    if (index == 0) {
		return head.element;
	}
	
	// Otherwise...
	else {
		
		// Starts from the head of the list.
		Node<E> testNode = head;
		E returnElement = head.element;
		
		// A variable to count the number of loops.
		int count = 0;
		
		// Uses a while loop to traverse the list up to the given index.
		while (count < index) {
			
			// Continues to the next Node. 
			testNode = testNode.next;
			
			// Resets the value of testNode.
			returnElement = testNode.element;
			
			// Increments count.
			count++;
		}
		// This loop will end once the list has been traversed and
		// returnElement equals the element at the given index.
		
		// Returns the element.
		return returnElement;
	}
  }

  @Override /** Return the index of the head matching element in 
   *  this list. Return -1 if no match. */
  public int indexOf(E e) {
    
    // If the list is empty, then the function returns -1.
    if (size == 0) {
		return -1;
	}
	
	// Otherwise...
	else {
		
		// A variable to store the current index.
		int index = 0;
		
		// Uses a for loop to traverse the list.
		for (E entry : this) {
			
			// If the entry is equal to the element.
			if (entry == e) {
				
				// Returns the index.
				return index;
			}
			
			// Increments index.
			index++;	
		}
		
		// If the element was not found in the list, then the 
		// function returns -1.
		return -1;
	}
  }


  @Override /** Return the index of the last matching element in 
   *  this list. Return -1 if no match. */
  public int lastIndexOf(E e) {
	
	// If the list is empty, returns -1.
    if (size == 0) {
		return -1;
	}
	
	// Otherwise...
	else {
		
		// A variable that will store the index of the last entry.
		int return_index = 0;
		
		// A variable to check if the entry is in the list.
		boolean found_entry = false;
		
		// Uses a for loop to traverse the list. 
		for (int index = 0; index < this.size(); index++) {
			
			// Retrieves the element at a given index.
			E entry = this.get(index);
			
			// If the entry is equal to e.
			if (entry == e) {
				
				// The entry was found in the list and the index
				// of the entry is stored. 
				found_entry = true;
				return_index = index;
			}
		}
		
		// If the entry was found in the list...
		if (found_entry) {
			
			// Returns the index of the last entry. 
			return return_index;
		}
		// Otherwise returns -1.
		else {
			return -1;
		}
	}
  }


  @Override /** Replace the element at the specified position 
   *  in this list with the specified element. */
  public E set(int index, E e) {
    
    // Checks if the given index is invalid. 
    boolean invalidIndex = index < 0 || index > size - 1;
    
    // If the index is invalid, returns a null value. 
    if (invalidIndex) {
		return null;
	} 
	
	// Otherwise...
	else {
		
		// Starts from the head of the list.
		Node<E> currentNode = head;
		
		// Uses a for loop to traverse the list. 
		for (int count = 0; count < index; count++) {
			
			currentNode = currentNode.next;
		}
		// When this loop ends, currentNode will represent the
		// node at the given index.
		
		// Replaces the element of currentNode. 
		currentNode.element = e;
		
		// Returns the elemnt of currentNode. 
		return currentNode.element;
	}
  }


  @Override /** Override iterator() defined in Iterable */
  public java.util.Iterator<E> iterator() {
    return new LinkedListIterator();
  }

  private void checkIndex(int index) {
    if (index < 0 || index >= size)
      throw new IndexOutOfBoundsException
        ("Index: " + index + ", Size: " + size);
  }
  
  private class LinkedListIterator 
      implements java.util.Iterator<E> {
    private Node<E> current = head; // Current index 
    
    @Override
    public boolean hasNext() {
      return (current != null);
    }

    @Override
    public E next() {
      E e = current.element;
      current = current.next;
      return e;
    }

    @Override
    public void remove() {
      System.out.println("Implementation left as an exercise");
    }
  }
  
  private static class Node<E> {
    E element;
    Node<E> next;

    public Node(E element) {
      this.element = element;
    }
  }
}

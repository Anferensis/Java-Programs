
/* Written by Albert Ong
 *
 * COMSC 76
 * Excercise 24.02
 */

public class Excercise_24_02 {
	
	public static void main(String args[]) {
		
		// Instantiates a LinkedList.
		MyLinkedList<String> list = new MyLinkedList<String>();
		list.add("A");
		list.add("B");
		list.add("C");
		list.add("D");
		list.add("E");
		list.add("C");
		
		// Prints out the list.
		System.out.println("Linked List: " + list + "\n");
		
		// The element to be tested. 
		String test_element = "C";
		
		// Tests the functions contains, get, indexOf, and lastIndexOf.
		System.out.println(
			"Contains " + test_element + ": " + list.contains(test_element));
		System.out.println(
			"Element at index 2: " + list.get(2));
		System.out.println(
			"Index of " + test_element + ": " + list.indexOf(test_element));
		System.out.println(
			"Last index of " + test_element + ": " + list.lastIndexOf(test_element));
		
		// Test the set method. 
		list.set(3, "ENTER");
		
		// Prints out the list after a new entry has been set.
		System.out.println("\nNew element set in list: " + list);
		
	}
}

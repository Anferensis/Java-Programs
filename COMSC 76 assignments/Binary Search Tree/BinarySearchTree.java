
/* Written by Albert Ong
 *
 * COMSC 76
 * Excercises 25.1, 25.6, and 25.7.
 */

public class BinarySearchTree {
	
	public static void main(String args[]) {
		
		// Creates a BST object. 
		Integer[] ints = {5, 3, 4, 6, 7};
		BST<Integer> tree = new BST<Integer>(ints);
		
		// Prints out elements from the BST via breadth-first traversal. 
		System.out.print("Breath of first traversal: ");
		tree.breadthFirstTraversal();
		
		// Prints out the height, the number of leaves, and the number
		// of non-leaves of the BST. 
		System.out.println("Height: " + tree.height());
		System.out.println("Number of leaves: " + tree.getNumberOfLeaves());
		System.out.println("Number of nonleaves: " + tree.getNumberOfNonLeaves());	
	}
}

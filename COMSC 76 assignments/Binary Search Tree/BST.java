
/* Written by Albert Ong
 *
 * COMSC 76
 * Excercises 25.1, 25.6, and 25.7.
 */
 
import java.lang.Math;
import java.util.Queue;
import java.util.LinkedList;

public class BST<E extends Comparable<E>> extends AbstractTree<E> {

	// Excercise 25.1
	/** Displays the nodes in a breadth-first traversal */
	public void breadthFirstTraversal() { 
		
		// If the BST is not empty...
		if (root != null) {
			
			// Uses a queue to store every node in each breadth of
			// the tree and starts with the root. 
			Queue<TreeNode<E>> queue = new LinkedList<TreeNode<E>>();
			queue.add(root);
			
			// A variable that determines if the queue is empty. 
			boolean queueNotEmpty = !queue.isEmpty();
			
			// As long as the queue is not empty...
			while (queueNotEmpty){
				
				// Checks if the left and right branches of the first 
				// element in the queue are null. 
				boolean leftNotNull = queue.element().left != null;
				boolean rightNotNull = queue.element().right != null;
				
				// Adds the left node of the root if it is not null. 
				if (leftNotNull) {
					queue.add(queue.element().left);
				}
				
				// Adds the right node of the root is it is not null
				if (rightNotNull) {
					queue.add(queue.element().right);
				}
				
				// Removes and prints out the element of the first node
				// in the queue. 
				System.out.print(queue.remove().element + " ");	
				
				// Resets the value of queueNotEmpty. 
				queueNotEmpty = !queue.isEmpty();
			}
			
			// Prints the end of the line. 
			System.out.println();
		}
	}
		
	
	/** Returns the height of this binary tree */
	public int height() {
		
		// Calls a recursive helper method. 
		return height(root);
	}
	
	public int height(TreeNode<E> root) {
		
		// The base case of the method.
		// Returns 0 if the inputted TreeNode is null. 
		if (root == null) {
			return 0;
		}
		else {
			
			// Recursive call that retrieves the left and right 
			// nodes of the root. 
			int left_height = height(root.left);
			int right_height = height(root.right);
			
			// The height of the tree will be one plus the
			// maximum height of either the left or right branches. 
			return 1 + Math.max(left_height, right_height);
		}
	}
	
	// Excercise 25.6
	/** Returns the number of leaf nodes */
	public int getNumberOfLeaves() {
		
		// Calls a recursive helper method. 
		return getNumberOfLeaves(root);
	}
	
	public int getNumberOfLeaves(TreeNode<E> root) {
		
		// The first base case.
		// Returns 0 if the inputted TreeNode is null.	
		if (root == null) {
			return 0;
		}
		
		// The second base case.
		// Returns 1 if both the left and right TreeNodes are null. 
		// This is because if both the left and right TreeNodes are
		// null, then the root is a leaf. 
		else if (root.left == null && root.right == null) {
			return 1;
		}
		
		else {
			// Recursively retrieves the number of leaves on the left
			// and right branches of the tree. 
			return getNumberOfLeaves(root.left) + 
				   getNumberOfLeaves(root.right);
		}
	}
	
	// Excercise 25.7
	/** Returns the number of nonleaf nodes */
	public int getNumberOfNonLeaves() {
		
		// If the tree is empty, then there are no non-leaves.
		if (root == null) {
			return 0;
		}
		else {
			// The number of leaves is the number of elements in the tree
			// minus the number of leaves. 
			return size - getNumberOfLeaves();
		}
	}


  protected TreeNode<E> root;
  protected int size = 0;

  /** Create a default binary tree */
  public BST() {
  }

  /** Create a binary tree from an array of objects */
  public BST(E[] objects) {
    for (int i = 0; i < objects.length; i++)
      insert(objects[i]);
  }

  @Override /** Returns true if the element is in the tree */
  public boolean search(E e) {
    TreeNode<E> current = root; // Start from the root

    while (current != null) {
      if (e.compareTo(current.element) < 0) {
        current = current.left;
      }
      else if (e.compareTo(current.element) > 0) {
        current = current.right;
      }
      else // element matches current.element
        return true; // Element is found
    }

    return false;
  }

  @Override /** Insert element o into the binary tree
   * Return true if the element is inserted successfully */
  public boolean insert(E e) {
    if (root == null)
      root = createNewNode(e); // Create a new root
    else {
      // Locate the parent node
      TreeNode<E> parent = null;
      TreeNode<E> current = root;
      while (current != null)
        if (e.compareTo(current.element) < 0) {
          parent = current;
          current = current.left;
        }
        else if (e.compareTo(current.element) > 0) {
          parent = current;
          current = current.right;
        }
        else
          return false; // Duplicate node not inserted

      // Create the new node and attach it to the parent node
      if (e.compareTo(parent.element) < 0)
        parent.left = createNewNode(e);
      else
        parent.right = createNewNode(e);
    }

    size++;
    return true; // Element inserted
  }

  protected TreeNode<E> createNewNode(E e) {
    return new TreeNode<E>(e);
  }

  @Override /** Inorder traversal from the root*/
  public void inorder() {
    inorder(root);
  }

  /** Inorder traversal from a subtree */
  protected void inorder(TreeNode<E> root) {
    if (root == null) return;
    inorder(root.left);
    System.out.print(root.element + " ");
    inorder(root.right);
  }

  @Override /** Postorder traversal from the root */
  public void postorder() {
    postorder(root);
  }

  /** Postorder traversal from a subtree */
  protected void postorder(TreeNode<E> root) {
    if (root == null) return;
    postorder(root.left);
    postorder(root.right);
    System.out.print(root.element + " ");
  }

  @Override /** Preorder traversal from the root */
  public void preorder() {
    preorder(root);
  }

  /** Preorder traversal from a subtree */
  protected void preorder(TreeNode<E> root) {
    if (root == null) return;
    System.out.print(root.element + " ");
    preorder(root.left);
    preorder(root.right);
  }

  /** This inner class is static, because it does not access 
      any instance members defined in its outer class */
  public static class TreeNode<E extends Comparable<E>> {
    protected E element;
    protected TreeNode<E> left;
    protected TreeNode<E> right;

    public TreeNode(E e) {
      element = e;
    }
  }

  @Override /** Get the number of nodes in the tree */
  public int getSize() {
    return size;
  }

  /** Returns the root of the tree */
  public TreeNode<E> getRoot() {
    return root;
  }

  /** Returns a path from the root leading to the specified element */
  public java.util.ArrayList<TreeNode<E>> path(E e) {
    java.util.ArrayList<TreeNode<E>> list =
      new java.util.ArrayList<TreeNode<E>>();
    TreeNode<E> current = root; // Start from the root

    while (current != null) {
      list.add(current); // Add the node to the list
      if (e.compareTo(current.element) < 0) {
        current = current.left;
      }
      else if (e.compareTo(current.element) > 0) {
        current = current.right;
      }
      else
        break;
    }

    return list; // Return an array of nodes
  }

  @Override /** Delete an element from the binary tree.
   * Return true if the element is deleted successfully
   * Return false if the element is not in the tree */
  public boolean delete(E e) {
    // Locate the node to be deleted and also locate its parent node
    TreeNode<E> parent = null;
    TreeNode<E> current = root;
    while (current != null) {
      if (e.compareTo(current.element) < 0) {
        parent = current;
        current = current.left;
      }
      else if (e.compareTo(current.element) > 0) {
        parent = current;
        current = current.right;
      }
      else
        break; // Element is in the tree pointed at by current
    }

    if (current == null)
      return false; // Element is not in the tree

    // Case 1: current has no left children
    if (current.left == null) {
      // Connect the parent with the right child of the current node
      if (parent == null) {
        root = current.right;
      }
      else {
        if (e.compareTo(parent.element) < 0)
          parent.left = current.right;
        else
          parent.right = current.right;
      }
    }
    else {
      // Case 2: The current node has a left child
      // Locate the rightmost node in the left subtree of
      // the current node and also its parent
      TreeNode<E> parentOfRightMost = current;
      TreeNode<E> rightMost = current.left;

      while (rightMost.right != null) {
        parentOfRightMost = rightMost;
        rightMost = rightMost.right; // Keep going to the right
      }

      // Replace the element in current by the element in rightMost
      current.element = rightMost.element;

      // Eliminate rightmost node
      if (parentOfRightMost.right == rightMost)
        parentOfRightMost.right = rightMost.left;
      else
        // Special case: parentOfRightMost == current
        parentOfRightMost.left = rightMost.left;     
    }

    size--;
    return true; // Element inserted
  }

  @Override /** Obtain an iterator. Use inorder. */
  public java.util.Iterator<E> iterator() {
    return new InorderIterator();
  }

  // Inner class InorderIterator
  private class InorderIterator implements java.util.Iterator<E> {
    // Store the elements in a list
    private java.util.ArrayList<E> list =
      new java.util.ArrayList<E>();
    private int current = 0; // Point to the current element in list

    public InorderIterator() {
      inorder(); // Traverse binary tree and store elements in list
    }

    /** Inorder traversal from the root*/
    private void inorder() {
      inorder(root);
    }

    /** Inorder traversal from a subtree */
    private void inorder(TreeNode<E> root) {
      if (root == null)return;
      inorder(root.left);
      list.add(root.element);
      inorder(root.right);
    }

    @Override /** More elements for traversing? */
    public boolean hasNext() {
      if (current < list.size())
        return true;

      return false;
    }

    @Override /** Get the current element and move to the next */
    public E next() {
      return list.get(current++);
    }

    @Override /** Remove the current element */
    public void remove() {
      delete(list.get(current)); // Delete the current element
      list.clear(); // Clear the list
      inorder(); // Rebuild the list
    }
  }

  /** Remove all elements from the tree */
  public void clear() {
    root = null;
    size = 0;
  }

}

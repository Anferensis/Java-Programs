
/* Written by Albert"Anferensis"Ong
 */


import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;



class TestWindow extends JFrame implements ActionListener {
	
	private ImageIcon image = new ImageIcon("test_image.png");
	private JLabel label = new JLabel(image);
	
	private JButton button = new JButton("Press to exit");
	private JPanel panel = new JPanel();
	
	
	public TestWindow() {
		
		setTitle("TestWindow");
		setSize(960, 540);
		
		add(label);
		
		button.addActionListener(this);
		panel.add(button);	
		add(panel);
		
		setVisible(true);	
	}
	
	
	public void actionPerformed(ActionEvent event) {
		
		System.out.println("Hello world!");
	}
}


public class JavaSwingTesting {
	
  public static void main(String[] args) {

	TestWindow test = new TestWindow();
  }
}


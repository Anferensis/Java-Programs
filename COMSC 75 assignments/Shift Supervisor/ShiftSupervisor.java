
/* Written by Albert Ong
 * 
 * COMSC 75
 * Programming assignment #10
 * 
 * Shift Supervisor
 */


class ShiftSupervisor extends Employee {
	
	// Creates two doubles to represent the annual salary and 
	// production bonus. 
	private double annual_salary;
	private double production_bonus;
	
	// The default constructor for the ShiftSupervisor class. 
	public ShiftSupervisor(String name, 
						   String num, 
						   String hire_date, 
						   double annual_sal, 
						   double prod_bonus) {	
		
		// Uses the super keyword to access the constructor for the
		// employee class. 
		super(name, num, hire_date);
		
		// Assigns variables exclusive to the ShiftSupervisor class. 
		annual_salary = annual_sal;
		production_bonus = prod_bonus;
	}
	
	// The no argument constructor.
	public ShiftSupervisor() {	
		annual_salary = 0.0;
		production_bonus = 0.0;
	}
	
	// Setter methods for the ShiftSupervisor class. 
	public void setAnnualSalary(double annual_sal) {
		annual_salary = annual_sal;
	}
	public void setProductionBonus(double prod_bonus) {
		production_bonus = prod_bonus;
	}
	
	// Getter methods for the Shift Supervisor class. 
	public double getAnnualSalary() {
		return annual_salary;
	}
	public double getProductionBonus() {
		return production_bonus;
	}
	
	// The toString method for the ShiftSuperisor class. 
	public String toString() {
		
		// Uses the toString method of the Employee class to 
		// convert the Employee object to a string. 
		String formatted_employee = super.toString() + "\n";
		
		// Formats both the annual salary and production bonus in to 
		// currency. Adds a '$' character in front, commas every three 
		// digits, and limits the number of decimal places to two. 
		String formatted_salary = 
			String.format("Annual Salary: $%,.2f \n", annual_salary);
		String formatted_bonus = 
			String.format("Production Bonus: $%,.2f \n", production_bonus);
		
		// Combines the formatted strings of the employee object, annual
		// salary, and production bonus. 
		String str = formatted_employee + formatted_salary + formatted_bonus;
		
		// Returns the final string. 
		return str;
	}
}



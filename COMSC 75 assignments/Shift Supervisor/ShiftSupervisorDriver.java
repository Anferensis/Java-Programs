
/* Written by Albert Ong
 * 
 * COMSC 75
 * Programming assignment #10
 * 
 * Shift Supervisor
 */


public class ShiftSupervisorDriver {
	
	public static void main(String args[]) {
		
		// Creates the first ShiftSupervisor object using a constructor. 
		ShiftSupervisor supervisor1 = new ShiftSupervisor("John Smith", 
														  "123-A", 
														  "11-15-2005", 
														  48000.00, 
														  6500.00);
		
		// Creates the second ShiftSupervisor object using setter 
		// methods from both the superclass and subclass. 
		ShiftSupervisor supervisor2 = new ShiftSupervisor();	 
		supervisor2.setName("Joan Jones");
		supervisor2.setEmployeeNumber("222-L");
		supervisor2.setHireDate("12-12-2005");
		supervisor2.setAnnualSalary(55000.00);
		supervisor2.setProductionBonus(8000.00);
		
		// Prints out the two ShiftSupervisor objects. 
		System.out.println(supervisor1);
		System.out.println(supervisor2);		
	}
}

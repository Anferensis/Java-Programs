
/* Written by Albert Ong
 * 
 * COMSC 75
 * Programming Assignment #7
 * 
 * Driver's Liscence 
 */
 
class DriverExam {
	
	// An array of the correct answers based upon the
	// assignments instructions. 
	private String[] correct_answers = 
		{"B", "D", "A", "A", "C", "A", "B", "A", "C", "D", 
		 "B", "C", "D", "A", "D", "C", "C", "B", "D", "A", };
	
	// An array used to store all of the user answers. 
	private String[] user_answers = new String[20];
	
	
	public DriverExam(String[] user_answers) {
		/* The constuctor for the DriverExam class.
		 * Only accepts one input: an array of strings.
		 */
		
		// Assigns the correct answers and user answers
		// as variables to the class. 
		this.correct_answers = correct_answers;
		this.user_answers = user_answers;
	}
	
	
	public boolean passedExam() {
		/* A method that checks whether or not the user
		 * passed the exam. 
		 */
		 
		// Find the total number of correct answers.
		int total_correct = this.totalCorrect();
		
		// Then checks if the total correct answers is
		// greater than 15. 
		boolean passed = total_correct > 15;
		
		// Returns the boolean variable "passed".
		return passed;
	}
	
	
	public int totalCorrect() {
		/* A method that calculates the total number of correct
		 * answers the user inputted.
		 */
		
		// Creates a integer that will be used to count the total
		// number of correct answers. 
		int correct_answers_count = 0;
		
		// Uses a for loop to examine all 20 items. 
		for (int index = 0; index < 20; index++) {
			
			// The integer "index" is used to find both the correct answer 
			// as well as the user answer for a given question. 
			String correct_answer = correct_answers[index];
			String user_answer = user_answers[index];
			
			// The answer is correct if the user answer equals the correct answer. 
			boolean isCorrect = user_answer.equals(correct_answer);
			
			// If the user answer was correct. 
			if (isCorrect) {
				
				// Increments the correct answers count. 
				correct_answers_count += 1;
			}		
		}	
		
		// Returns the total number of correct answers. 	
		return correct_answers_count;
	}
	
	
	public int totalIncorrect() {
		/* A method that calculates the total number of incorrect
		 * answers the user inputted. 
		 */
		
		// Uses the method totalCorrect to find the total number
		// of correct answers. 
		int total_correct = this.totalCorrect();
		
		// The difference between 20, the total number of questions,
		// and the total correct will be the number of incorrect answers. 
		int total_incorrect = 20 - total_correct;
		
		// Returns the total incorrect. 
		return total_incorrect;
	}
	
	
	public int[] questionsMissed() {
		/* A method that assembles an array of integers that represent
		 * the number of each question missed.
		 */
		
		// Find the total number of incorrect answers. 
		int total_incorrect = this.totalIncorrect();
		
		// The size of the questions_missed array is based upon the
		// tota number of incorrect answers. 
		int[] questions_missed = new int[total_incorrect];
		
		// Creates an integer to represent the index of the next
		// item in the questions_missed array. 
		int question_index = 0;
		
		// Uses a for loop to examine all 20 items. 
		for (int index = 0; index < 20; index++) {
			
			// The integer "index" is used to find both the correct answer 
			// as well as the user answer for a given question. 
			String correct_answer = correct_answers[index];
			String user_answer = user_answers[index];
			
			// The answer is correct if the user answers equals
			// the correct answer. 
			boolean isCorrect = user_answer.equals(correct_answer);
			
			// If the answer is not correct...
			if (!isCorrect) {
				
				// The question number is the index plus 1.
				int question_number = index + 1;
				
				// Assigns the question nnumber to the questions
				// missed index. 
				questions_missed[question_index] = question_number;
				
				// Increments the question index, because the next item
				// will be one index place higher. 
				question_index += 1;
			}		
		}	
		
		// Returns the array of questions missed. 
		return questions_missed;
	}
}


/* WordGame.java
 * 
 * Written by Albert Ong
 * 
 * Programming Assignment #2
 * 
 * A program designed to ask for information from the user 
 * and convert that information into a story. 
 */


public class WordGame {

   public static void main(String args[]) {
	    
	    // Asks the user what his/her name is
		String user_name = System.console().readLine("Enter your name: ");
		
		// Asks the user what his/her age is
		String user_age = System.console().readLine("Enter your age: "); // Need to fix, must only accept integer
		
		// Asks the user for a city
		String user_city = System.console().readLine("Enter the name of a city: ");
		
		// Asks the user for a college
		String user_college = System.console().readLine("Enter the name of a college: ");
		
		// Asks the user for a profession
		// The variable is lowercased for grammatical reasons. 
		String user_profession = 
			System.console().readLine("Enter profession: ").toLowerCase();
		
		// Asks for a type of pet animal
		// The variable is lowercased for grammatical reasons
		String user_pet_type = 
			System.console().readLine("Enter a type of animal: ").toLowerCase();
		
		// Asks for the name of that pet animal
		String user_pet_name = System.console().readLine("Enter a pet name: ");

		
		// Assembles all of the input in to a meaningful story. 		
		String output = 
			"There once was a person named " + user_name + " who lived in " + user_city + ". " 
		  + "At the age of " + user_age + ", " + user_name +" went to college at " + user_college + ". "
		  + user_name + " graduated and went to work as a " + user_profession + ". "
		  + "Then, " + user_name +" adopted a(n) " + user_pet_type +" named " + user_pet_name + ". " 
		  + "They both lived happily ever after!";
		
		// Prints out the story. 			  
		System.out.println("\n" + output);
   }
} 


/* Written by Albert Ong
 * 
 * COMSC 75
 * Programming assignment #9
 * 
 * Miscellaneous String Operations
 */

public class MiscellaneousStringOperations {
	
	public static int wordCount(String string) {
		/* A method that counts the number of words in a string.
		 */
		
		// Creates an array of strings by splitting the string
		// at every blank space. 
		String[] split_string = string.split(" ");
		
		// Calculates the number of words based of the length
		// of the split string. 
		int number_of_words = split_string.length;
		
		// Returns the number of words. 
		return number_of_words;
	}
	
	
	public static String arraytoString(char[] characters) {
		/* A method that converts an array of characters to a string.
		 */
		return String.valueOf(characters);
	}
	
	
	public static char mostFrequent(String string) {
		/* A method that finds the most common character in a string.
		 */
		
		// Converts the string to an array of characters
		// This is for for looping.  		
		char[] char_array = string.toCharArray();
		
		// Creates two variables, one to represent the most frequent character
		// and another to represent the number of occurences of the most
		// frequent character. 
		char most_frequent = ' ';
		int most_occurences = 0;
		
		// For loops each character in the string. 
		for (char char1 : char_array) {
			
			// A variable to count the occurences of each character. 
			int count = 0;
			
			// Uses a for loop to count the number of occurences of char1.  
			for (char char2 : char_array) {
				
				// If the characters are equal
				if (char1 == char2) {
					
					// Increments the count. 
					count += 1;
				}
			}
			// By the end of this for loop, count will equal the number of times
			// a character occurs in the string. 
			
			// If count is greater than the current value of most occurences. 
			if (count > most_occurences) {
				
				// Sets most frequent equal to char1. 
				most_frequent = char1;
				
				// Sets most frequent to the number of occuences of char1. 
				most_occurences = count;
			}		
		}
		
		// Returns the most frequent character. 
		return most_frequent;
	}
	
	public static String replaceSubstring(String string1, String string2, String string3) {
		/*  A method that replaces every occurence of string2 within string1 with string3. 
		 */
		return string1.replace(string2, string3);
	}
	
	public static void main(String args[]) {
		
		String string = "the dog jumped over the fence";
		
		// Demonstrating the wordCount method.
		System.out.println("Number of words in \"the dog jumped over the fence\" is " + wordCount(string));
		
		// Demonstrating the mostFrequent method.
		System.out.println("Most frequently occuring character: " + mostFrequent(string));
		
		// Demonstrating the replaceSubstring method.
		System.out.println(replaceSubstring(string, "the", "that"));
		
		// Demonstating the arraytoString method. 
		char[] char_array = {'H', 'o', 'w', ' ', 'n', 'o', 'w', ' ', 'b', 'r', 'o', 'w', 'n', ' ', 'c', 'o', 'w'};
		String array_to_string = arraytoString(char_array);
		System.out.println(array_to_string);
	}
}



/* Written by Albert Ong
 * 
 * COMSC 75
 * Programming Assignment #6
 * 
 * Constructs and prints out three Employee objects.
 */

public class EmployeeDriver {

	public static void main(String[] args) {
	
		// Assembles three Employee objects based on
		// the assignment instuctions. 
		
		// Uses the no argument constructor.
		Employee employee1 = new Employee();
		employee1.setName("Susan Meyers");
		employee1.setIDNumber(47899);
		employee1.setDepartment("Accounting");
		employee1.setPosition("Vice President");

		
		// Uses the default constructor.
		Employee employee2 = new Employee("Mark Jones",
										  39119,
										  "IT",
										  "Programmer");									  
		
		// Uses the partial contructor.
		Employee employee3 = new Employee("Joy Rodgers", 
										   81774);										  
		employee3.setDepartment("Manufacturing");
		employee3.setPosition("Engineering");
		
		
		// Creates a list of the three Employee objects
		Employee[] employees = {employee1, employee2, employee3};
		
		// Uses a for loop to print out the Employee objects.
		for (int index = 0; index < 3; index++) {
			
			// Indexes the list for an Employee object.
			Employee employee;
			employee = employees[index];
			
			// Prints out the Employee number.
			int employee_num = index + 1;
			System.out.println("Employee: #" + employee_num);
			
			// Prints the Employee object itself.
			System.out.println(employee);
		}
	}
}


/* Written by Albert Ong
 * 
 * 
 * A program designed to print out every prime number
 * between 1 and 100. 
 */

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;


public class PrimeNumbersList {
	
	public static boolean isPrime(int num) {
		
		boolean is_prime = true; 
		
		if (num % 2 == 0 & num != 2) {		
			is_prime = false; 
		}
		
		for(int x = 3; x*x <= num; x += 2) {
			
			if (num % x == 0) {
				is_prime = false;
			}
		}
			
		return is_prime;
	}
	
	
	
	public static void main(String args[]) throws IOException {
		
		String output = "";
		
		for (int num = 1; num < 101; num++) {
		
			if (isPrime(num)) {
				
				String num_str = String.valueOf(num);
				output += num_str + "\n";
			}		
		}
		
		String file_name = "PrimeNumbersList.txt";
		
        BufferedWriter writer = 
			new BufferedWriter(new FileWriter(file_name));				
		
		writer.write(output);
        writer.close();	
        
        
        // Prints out the output and informs the user that the
        // output has been saved to a text file. 
        // This is for informational purposes only. 
        System.out.println(output);
        System.out.println("Text saved to \"" + file_name + "\"");
	}	
} 

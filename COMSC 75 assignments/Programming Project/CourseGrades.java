
/* Written by Albert Ong
 *
 * COMSC 75
 * Programming assignment #12
 * 
 * Programming Project
 */


public class CourseGrades implements Analyzable {
	
	// Creates an array of GradedActivity objects named grades. 
	private GradedActivity[] grades;
	
	// The contructor for the CourseGrades object. 
	public CourseGrades() {

		// Sets the length of the array to 4. 
		// This array will be used to store a lab, pass-fail exam, 
		// essay, and final exam. 	
		grades = new GradedActivity[4];
	}
	
	// The setter methods for the CourseGrades object. 
	public void setLab(GradedActivity lab) {
		grades[0] = lab;
	}	
	public void setPassFailExam(PassFailExam pass_fail_exam) {
		grades[1] = pass_fail_exam;
	}	
	public void setEssay(Essay essay) {
		grades[2] = essay;
	}
	public void setFinalExam(FinalExam final_exam) {
		grades[3] = final_exam;
	}
	
	// The getter methods for the CouseGrades object. 
	public GradedActivity getLab() {
		return grades[0];
	}
	public GradedActivity getPassFailExam() {
		return grades[1];
	}
	public GradedActivity getEssay() {
		return grades[2];
	}
	public GradedActivity getFinalExam() {
		return grades[3];
	}
	
	
	// A method that returns the average grade from grades. 
	public double getAverage() {
		
		// Creates two doubles, one to represent the average grade and
		// another to store the sum of all grades. 
		double average, sum = 0.0;

		// Uses a for loop to add the sum of all the grades. 
		for (GradedActivity act : grades) {
			double score = act.getScore();
			sum += score;
		}
		
		// Retrieves the number of assignments (which is always 4).
		double number_of_items = grades.length;
		
		// Calculates the average by dividing the sum by the number of items. 
		average = sum / number_of_items;
		
		// Returns the average grade. 
		return average;
	}
	
	
	// A method that returns the highest grade from grades. 
	public GradedActivity getHighest() {
		
		// Creates two variables, a GradedActivity object that will store 
		// the highest activity and a double to store the highest grade. 
		GradedActivity highest_activity = new GradedActivity();
		double highest_grade = 0.0;
		
		// Uses a for loop to analyze each GradedActivity.
		for (GradedActivity act : grades) {
			
			// Retrives the score of the GradedActivity. 
			double score = act.getScore();
			
			// If the score is higher than the current value of highest_grade...
			if (score > highest_grade) {
				
				// Resets the value of highest grade. 
				highest_grade = score;
				highest_activity = act;
			}
		}
		// By the end of this for loop, highest_grade will represent the 
		// highest grade in grades. 
		
		// Returns the highest graded activity. 
		return highest_activity;
	}
	
	
	// A method that returns the lowest grade from grades. 
	public GradedActivity getLowest() {
		
		// Creates two variables, a GradedActivity object that will store
		// the lowest activity and a double that will store the lowest grade.
		GradedActivity lowest_activity = new GradedActivity();
		double lowest_grade = 100.0;
		
		// Uses a for loop to analyze each GradedActivity in grades. 
		for (GradedActivity act : grades) {
			
			// Retrieves the score of the activity. 
			double score = act.getScore();
			
			// If the score of the activity is lower than the lowest grade...
			if (score < lowest_grade) {
				
				// Reassigns the values of lowest_grade and lowest_activity
				lowest_grade = score;
				lowest_activity = act;
			}
		}
		// By the end of this for loop, lowest_activity will represent 
		// the activity will the lowest score. 
		
		// Returns the lowest graded activity. 
		return lowest_activity;
	}
	
	
	// The toString method of the CourseGrades object. 
	public String toString() {
		
		// Creates a string that will become the final string returned
		// at the end of the method. 
		String string = "";
		
		// An array of all the activity names. 
		// This is important for the for loop afterwards. 
		String[] activity_names = {"Lab", "Pass/Fail Exam", "Essay", "Final Exam"};
		
		// Uses a for loop to for mat each activity into a string. 
		for (int index = 0; index < 4; index++) {
			
			// Retrives the activity from grades and the activity name
			// from activity_names. 
			GradedActivity activity = grades[index];
			String activity_name = activity_names[index];
			
			// Retrives the number grade and the letter grade of the activity. 
			String number_grade = Double.toString(activity.getScore());
			String letter_grade = Character.toString(activity.getGrade());
			
			// Formats the activity name, number grade, and letter grade 
			// into a string names activity_line. 
			String activity_line = 
				activity_name + " Score: " + number_grade + " Grade: " + letter_grade + "\n";
			
			// Adds activity_line to the finall string. 
			string += activity_line;
		}
		
		// Retrieves the average, highest, and lowest scores of the
		// CourseGrades object. 
		double average_score = this.getAverage();
		double highest_score = this.getHighest().getScore();
		double lowest_score = this.getLowest().getScore();
		
		// Formats the average, highest, and lowest scores into a string. 
		String average_line = "Average Score: " + Double.toString(average_score) + "\n";
		String highest_line = "Highest Score: " + Double.toString(highest_score) + "\n";
		String lowest_line = "Lowest Score: " + Double.toString(lowest_score) + "\n";
		
		// Adds each line to the finalized string. 
		string += average_line + highest_line + lowest_line; 
		
		// By this point, string will contain a line for each GradedActivity
		// in grades as well as a line for the average, highest, and
		// lowest score. 
		
		// Returns the finalized string. 
		return string;
	}
}

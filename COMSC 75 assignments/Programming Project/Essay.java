
/* Written by Albert Ong
 *
 * COMSC 75
 * Programming assignment #12
 * 
 * Programming Project
 */

class Essay extends GradedActivity {
	
	// Creates four private fields, one to represent each criteria of
	// grading an essay: grammar, spelling, length, and content. 
	private double grammar;
	private double spelling;
	private double correct_length;
	private double content;
	
	// The constructor for the Essay object.
	// Accepts four integers as inputs. 
	public Essay (double g, double s, double cl, double cn) {
		
		// Uses the setter methods to assign each inpout to a 
		// given field of the class. 
		setGrammar(g);
		setSpelling(s);
		setCorrectLength(cl);
		setContent(cn);
		
		// Adds together each criteria to calculate the total score. 
		double total_score = grammar + spelling + correct_length + content;
		
		// Uses the setScore method from the GradedActivity class to
		// assign the Essay class a score. 
		setScore(total_score);
	}
	
	// The setter methods of the Essay object. 
	public void setGrammar(double g) {
		
		if (g > 30.0) {
			grammar = 30.0;
		}
		else {
			grammar = g;
		}
	}
	public void setSpelling(double s) {
		
		if (s > 20.0) {
			spelling = 20.0;
		}
		else {
			spelling = s;
		}
	}
	public void setCorrectLength(double cl) {
		
		if (cl > 20.0) {
			correct_length = 20.0;
		}
		else {
			correct_length = cl;
		}
	}
	public void setContent(double cn) {
		
		if (cn > 30.0) {
			content = 30.0;
		}
		else {
			content = cn;
		}
	}
	
	
	// The getter methods of the Essay object. 
	public double getGrammar() {
		return grammar;
	}
	public double getSpelling() {
		return spelling;
	}
	public double getCorrectLength() {
		return correct_length;
	}
	public double getContent() {
		return content;
	}
}


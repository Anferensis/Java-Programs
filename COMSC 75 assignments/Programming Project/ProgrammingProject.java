
/* Written by Albert Ong
 *
 * COMSC 75
 * Programming assignment #12
 * 
 * Programming Project
 * 
 * Total number of files used:
 * 		1. ProgrammingProject 	 
 * 			Personally built. The driver program of the assignment. 
 * 		2. CourseGrades 		 
 * 			Personally built. Assembles a lab, essay, pass-fail exam, 
 * 			and final exam into a single object. 
 * 		3. Analyzable 			 
 * 			Given. Forces CourseGrades to contain three methods:
 * 			getAverage() - which returns the average score as a double,
 * 			getHighest() - which returns the highest GradedActivity
 * 			getLowest() - which returns the lowest GradeActivity. 
 * 		4. GradedActivity 		 
 * 			Given. The parent class of Essay, PassFailActivity, 
 * 			FinalExam, and PassFailExam. 
 * 		5. Essay 				 
 * 			Personally built. A subclass of GradedActivity.  
 * 		6. PassFailActivity 	 
 * 			Given. A subclass of GradedActivity. 
 * 		7. FinalExam 			 
 * 			Given. A subclass of GradedActivity. 
 * 		8. PassFailExam 		 
 * 			Given. A subclassof FinalExam and GradedActivity. 
 */


public class ProgrammingProject {
	
	public static void main(String args[]) {
		
		// Creates a GradedActivity object, which represents a lab, 
		// and assigns that object a score. 
		GradedActivity lab = new GradedActivity();
		lab.setScore(85.0);
		
		// Creates a PassFailExam object. 
		PassFailExam pass_fail_exam = new PassFailExam(100, 15, 70);		

		// Creates an Essay object. 
		Essay essay = new Essay(20, 10, 20, 30);
		
		// Creates a FinalExam object. 
		FinalExam final_exam = new FinalExam(50, 10);
		
		// Constructs a CourseGrades object using the four objects
		// created above. 
		CourseGrades course_grades = new CourseGrades();
		course_grades.setLab(lab);
		course_grades.setPassFailExam(pass_fail_exam);
		course_grades.setEssay(essay);
		course_grades.setFinalExam(final_exam);
		
		// Prints out the CourseGrades object. 
		System.out.println(course_grades);
	}
}



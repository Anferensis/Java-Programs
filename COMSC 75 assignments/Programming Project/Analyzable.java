
/* Written by Albert Ong
 *
 * COMSC 75
 * Programming assignment #12
 * 
 * Programming Project
 */


public interface Analyzable {
	
	double getAverage();
	GradedActivity getHighest();
	GradedActivity getLowest();
}



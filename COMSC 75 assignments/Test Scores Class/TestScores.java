
/* Written by Albert Ong
 *
 * COMSC 75
 * Programming assignment #11
 * 
 * Test Scores Class
 */


class TestScores {
	
	// Creates a single private field: an array on integers designed
	// to represent a list of scores. 
	private int[] test_scores;
	
	// The defualt constructor for the TestScores class. 
	public TestScores(int[] scores) {
		
		// A variable to check if all scores are valid. 
		boolean is_valid_score = true;
		
		// Uses a for loop to check each score. 
		for (int score: scores) {
			
			// If a score is either less than 0 or greater than 100...
			if (score < 0 || score > 100) {
				
				// The score is not valid. 
				is_valid_score = false;
				break;
			}
		}
		
		// If all scores are valid...
		if (is_valid_score) {
			
			// The input is assigned to the class. 
			test_scores = scores;
		}
		
		// Otherwise..
		else {
			
			// Returns an IllegalArgumentException. 
			String error_message = 
				"Invalid input. All inputted scores must be less " +
				"or equal to 100. ";		
			throw new java.lang.IllegalArgumentException(error_message);
		}
	}
	
	// The no argument constructor for the TestScroes class.
	public TestScores() {
		test_scores = null;
	}
	
	// A method that will return the average score. 
	public int getAverage() {
		
		// A variable to represent the sum of all the scores 
		int sum = 0;
		
		// Uses a for loop to add each score to the sum. 
		for (int score : test_scores) {
			sum += score;
		}
		// By the end of this for loop, sum will be the total of all scores. 
		
		// Retrieves the number of items in test_scores
		int number_of_items = test_scores.length;
		
		// Calculates the average by dividing the sum by the
		// number of items. 
		int average = sum / number_of_items;
		
		// Returns the average. 
		return average; 
	}
	
}



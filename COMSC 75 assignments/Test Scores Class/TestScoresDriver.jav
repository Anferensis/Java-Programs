
/* Written by Albert Ong
 *
 * COMSC 75
 * Programming assignment #11
 * 
 * Test Scores Class
 */


public class TestScoresDriver {
	
	public static void main(String args[]) {
		
		// Creates a TestScores object.
		int[] scores = {100, 96, 70, 87, 78};	
		TestScores testScores = new TestScores(scores);
		
		// Prints out the average score of the TestScores object. 
		int average = testScores.getAverage();
		System.out.println("The average score is " + average + ".\n");	
	}
}




/* Written by Albert Ong
 * 
 * Programming assignment #3
 * COMSC 75
 * 
 * A program designed to recieve two inputs, customer package type and customer hours,
 * and then return the cost. 
 */

public class InternetServiceProvider {

	public static void main(String args[]) {
		
		// Instantiates a string to represent the customer package type.
		String customer_package = ""; 
		
		// Instantiates a boolean to represent whether the user
		// has entered a valid input
		boolean validInput = false;
		
		
		// While validInput is false
		while (!validInput) {
			
			// Asks the user for the package type, either "A", "B", or "C".
			customer_package = System.console().readLine("Enter the customer's package: ");
			
			// Checks if the input is equal to "A", "B", or "C"
			boolean isABC = customer_package.equals("A") || 
							customer_package.equals("B") ||
							customer_package.equals("C");
			
			// If so...			
			if (isABC) {
				
				// Then the user entered a valid input. 
				validInput = true;
				// Automatically exits from the while loop. 
			} 
			
			// Otherwise...
			else {
				
				// Prints out an error message. 
				System.out.println("Invalid input. The package type must be either \"A\", \"B\", or \"C\". ");
				// The while loop will continue, until the user enters either "A", "B", or "C"
			}		
		}
		
			
		// Asks the user for the number of hours.
		String customer_hours = System.console().readLine("Enter the number of hours used: ");
		
		// Converts the customer hours from a string to a double.
		// This is used for later calculations. 
		double customer_hours_num = Double.parseDouble(customer_hours);
		
		// Instantiates a double named "cost" with the value 0
		// This variable will be altered based upon the previous input. 
		double cost = 0.00; 
		
		
		// Uses a switch-case statement to distinguish the possible
		// customer packages: A, B, and C
		switch (customer_package) {
			
			
			// If the package type equals "A"
			case "A":
				
				// The initial cost will be $9.95
				cost = 9.95;
				
				// However, is the number of hours is above 10...
				if (customer_hours_num > 10) {
					
					// Calculates and adds an additional cost
					double additional_cost = (customer_hours_num - 10) * 2.00;				
					cost += additional_cost;
				}			
				break;
			
			
			// If the package type equals "B"
			case "B":
			
				// The initial cost will be $13.95. 
				cost = 13.95;
				
				// If the number of hours is above 20...
				if (customer_hours_num > 20) {
					
					// Calculates and adds an additional cost. 
					double additional_cost = (customer_hours_num - 20) * 1.00;				
					cost += additional_cost;
				}
				break;
			
					
			// If the package type equals "C"
			case "C": 
			
				// The cost will always be $19.95
				cost = 19.95;
				
				break;
		}
		
		// Returns the final cost. 
		System.out.printf("\nThe charges are $%.2f.", cost);
   }
} 


/* Written by Albert Ong
 * 
 * COMSC 75
 * Programming Assignment #6
 * 
 * A object designed to represent an Employee.
 */

class Employee {
	
	// Creates four variables: 
	// 		name, id number, department, and position
	private String name;
	private int id_number;
	private String department;
	private String position;
	
	// The no argument constructor of the Employee class.
	public Employee() {
		this.name = "";
		this.id_number = 0;
		this.department = "";
		this.position = "";
	}
	
	// The partial constructor for the Employee class.
	// Only accepts a name and ID number.
	public Employee(String name, 
					int id_number) {
		
		this.name = name;
		this.id_number = id_number;
		this.department = "";
		this.position = "";
	}
	
	// The default constructor for the Employee class.
	public Employee(String name,
					int id_number,
					String department,
					String position) {
		
		// Assigns the name, id number, department, and position variables.
		this.name = name;
		this.id_number = id_number;
		this.department = department;
		this.position = position;
	}
	

	
	// Creates a printable string for the Employee class
	public String toString() {
		
		// Creates a string variable named "output"
		// This will become the final output that is returned. 
		String output = "";
		
		// Converts the id number to a string
		// This is for printing purposes. 
		String id_number_str = Integer.toString(id_number);
		
		// Assembles a list of strings
		// Each item in the list represents a line in the final string.
		String[] items = {"Name: " + name,
						  "ID Number: " + id_number_str,
						  "Department: " + department,
						  "Position: " + position};
		
		// Uses a for loop to assemble the output.
		for (int index = 0; index < 4; index++) {
			
			// Appends each value in a separate line
			String item = items[index];
			output += item + "\r\n";
		}

		// Returns the final output.
		return output;
	}
	
	// Mutator methods for all four variables.
	public void setName(String name) {
		this.name = name;
	}
	public void setIDNumber(int id_number) {
		this.id_number = id_number;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	
	// Accessor methods for all four variables. 
	public String getName() {
		return this.name;
	}
	public int getIDNumber() {
		return this.id_number;
	}
	public String getDepartment() {
		return this.department;
	}
	public String getPosition() {
		return this.position;
	}
}

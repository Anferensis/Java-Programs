
/* Written by Albert Ong
 * 
 * COMSC 75
 * Extra credit assignment. 
 */

import java.util.ArrayList;
import java.util.Scanner;


public class PhoneBookDriver {
	 
	public static void main(String args[]) {
		
		System.out.println("Please enter 5 names and phone numbers. \n");
		
		ArrayList<PhoneBookEntry> list = new ArrayList<PhoneBookEntry>();
		Scanner scanner = new Scanner(System.in);
		
		String name;
		String phone_number;
			
		for (int count = 0; count < 5; count++) {
			
			// Get data
			System.out.print("Enter a person's name: ");
			name = scanner.nextLine();	
			System.out.print("Enter that person's phone number: ");
			phone_number = scanner.nextLine();
			
			// Creates a PhoneBookEntry object based upon the input.
			PhoneBookEntry entry = new PhoneBookEntry(name, phone_number);
			
			// Adds object to the ArrayList.
			list.add(entry);
		}
			
		System.out.println("\nHere's the data you entered: ");
		
		// Uses an enhanced for loop to print out the contents of the ArrayList. 
		for (PhoneBookEntry entry : list) {		
			System.out.println("------------------------");
			System.out.println("Name: " + entry.getName());
			System.out.println("Phone Number: " + entry.getPhoneNumber());

		}
	}	
}


/* Written by Albert Ong
 * 
 * COMSC 75
 * Extra credit assignment. 
 */

public class PhoneBookEntry {
	 
	 private String name;
	 private String phoneNumber;
	 
	 // Constructor for the PhoneBookEntry class.
	 // Accepts two inputs: a name and a phone number.
	 // Both are represented as strings.
	 public PhoneBookEntry(String n, String pn) {
		 name = n;
		 phoneNumber = pn;
	 }
	 
	 // Setter methods for the PhoneBookEntry class.
	 public void setName(String n) {
		 name = n;
	 } 
	 public void setPhoneNumber(String pn) {
		 phoneNumber = pn;
	 }
	 
	 // Getter methods for the PhoneBookEntry class.
	 public String getName() {
		 return name;
	 } 
	 public String getPhoneNumber() {
		 return phoneNumber;
	 }
}

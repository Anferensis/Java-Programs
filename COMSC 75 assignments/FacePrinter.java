
/* Written by Albert Ong
 * 
 * FacePrinter.class
 * Programming Assignment #1
 * 
 * A class designed to print out a text face.
 */

public class FacePrinter {

   public static void main(String args[]) {
	   
      String output = " / / / / / / \n" 
					+ " |  0   0  | \n"
					+ "(|    C    |) \n" 
					+ " | ( ___ ) | \n"
					+ " \\_________/";
					
      System.out.println(output);
   }
} 


/* Written by Albert Ong
 * 
 * COMSC 75
 * Programming Assignment #7
 * 
 * Driver's Liscence 
 */
 
 
public class DriversLicense {
	
	
	/* The driver function of the program.
	 * 
	 * Accepts the user's input for all 20 questions and then
	 * uses the DriverExam class to return the desired output. 
	 */
	public static void main(String args[]) {
		
		// Creates an used array to store all of the user answers. 
		String[] user_answers = new String[20];
		
		// Uses a for loop ask the user 20 questions. 
		for (int question_num = 1; question_num < 21; question_num++) {
			 
			 // A variable to represent whether or not the user's input is
			 // either "A", "B", "C", or "D".
			 boolean isABCD = false;
			 
			 // A while loop that will run indefinitely until the user inputs
			 // either "A", "B", "C", or "D"
			 // Becasue isABCD is false by default, this loop will run atleast once. 
			 while (!isABCD) { 
				 
				// Prompts the user to answer a question
				String answer;
				answer = System.console().readLine("Question " + question_num + ": ");
				
				// Checks if the user's answer is either "A", "B", "C", or "D". 
				isABCD = answer.equals("A") ||
						 answer.equals("B") ||
						 answer.equals("C") ||
						 answer.equals("D");
				
				// If the user inputted the correct input...
				if (isABCD) { 
					
					// Assigns the user answer to the user_answers array. 
					int index = question_num - 1;
					user_answers[index] = answer;
				}
				
				// Otherwise...
				else {
					
					// Tells the user to enter a valid input. 
					System.out.println(
						"Invalid input. You must enter either \"A\", \"B\", \"C\" or \"D\". \n" );
				}
				
				// The while loop will either break if isABCD is true or 
				// loop again if isABCD is false. 
			}
		}
		
		// Prints a blank line. 
		// This is to separate the questions above from the
		// output below. 
		System.out.println();
		
		// Creates a DriverExam object. 
		DriverExam exam = new DriverExam(user_answers);
		
		// Uses the totalCorrect and totalIncorrect methods to
		// inform the user of how many questions were answered
		// correctly or incorrectly. 
		System.out.println("Correct answers: " + exam.totalCorrect());
		System.out.println("Incorrect answers: " + exam.totalIncorrect());
		
		// Uses the passedExam method to determine if the
		// user passed the exam. 
		boolean passed = exam.passedExam();
		
		// Tells the user if he/she passed or failed based on
		// the value of "passed".
		if (passed) {
			System.out.println("You passed the exam.");
		}
		else {
			System.out.println("You failed the exam.");
		}
		

		
		// Prints out the first part of the questions missed line.
		System.out.print("You missed the following questions: ");
		
		
		// Uses the questionsMissed method to get the array or integers
		// representing the questions miseed and retrieves the
		// length of said array.
		int[] questions_missed = exam.questionsMissed();
		int length = questions_missed.length;
		
		// Prints "none" if the user missed no questions
		if (length == 0) {
			System.out.print("none");
		}
		
		// Otherwise...
		else {
			// Uses a for loop to print out every item from the 
			// array questions missed. 
			for (int index = 0; index < length; index++) {
				
				// Prints out the question missed along with a space after. 
				System.out.print(questions_missed[index] + " ");
			}
		}
		
		// Prints out a blank line at the end.
		// This is for formatting purposes. 
		System.out.println("\n");
	}
}

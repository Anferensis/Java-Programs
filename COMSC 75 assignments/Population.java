
/* Written by Albert Ong
 *
 * COMSC 75
 * Programming Assignment #4
 * 
 * A program designed to predict the size of a population of organisms.
 * The program requires three inputs: a starting number of organisms, 
 * average daily population increase, and the number of days the 
 * organisms will multiply. 
 * 
 * The starting number must be greater than or equal to two, the daily population
 * increase must be greater than or equal to zero, and the number of days must
 * be greater than or equal to one. 
 * 
 * When run, the program will: 
 * 		1. Ask the user for a starting number or organisms, 
 * 		   daily increase, and number of days.
 * 		2. Print out a chart displaying the total number of 
 * 		   organisms per day. 
 * 		3. Save the chart in a separate text file. 
 */


// Importing the necessary code. 
// The io objects are for saving a separate text file, 
// whereas the Scanner object is for registering input. 
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;


public class Population {
	
	// The main function is declared here.
	
	// Note: The main function will automatically handle IOExceptions. 
	// 		 This is important when writing a separate text file. 
	public static void main (String[] args) throws IOException {
		
		// Instantiates a scanner object. 
		Scanner scanner = new Scanner(System.in);

		// Instantiates a starting number with the value of three
		// (the exact value does not matter as long as it is less than 2)
		double starting_number = 0;
		
		// This while loop will run indefinitely, until the user inputs 
		// a starting number greater than or equal to 2. 
		while (starting_number < 2) {
			
			// Asks the user for a starting number of organisms. 
			System.out.print("Enter the starting number of organisms: ");
			starting_number = scanner.nextDouble();
			
			// Prints a message if the user entered an invalid input. 
			if (starting_number < 2) {
				System.out.println("Invalid input. Please enter a number greater than or equal to 2. \n");
			}
		}
		
		// Instantiates the daily increase rate with the value -1.0. 
		// (the exact value does not matter as long as it is less than 0.0)				
		double daily_increase = -1.0; 
		
		// This while loop will run indefinitely, until the user inputs 
		// a daily increase greater than or equal to 0.0. 
		while (daily_increase < 0.0 ) {
			
			// Asks the user for a  rate of daily increase. 
			System.out.print("Enter the daily increase: ");
			daily_increase = scanner.nextDouble();	
			
			// Prints a message if the user entered an invalid input. 
			if (daily_increase < 0.0) {
				System.out.println("Invalid input. The daily increase must be greater than or equal to 0.0. \n");
			}
		}
		
		// Instantiates the number of days with the value of 0. 
		// (the exact value does not matter as long as it is less than 1)			
		int days = 0;
		
		// This while loop will run indefinitely, until the user inputs 
		// a number of days greater than or equal to 1. 
		while (days < 1) {
			
			// Asks the user for a number of days the organisms will multiply.  
			System.out.print("Enter the number of days the organisms will multiply: ");
			days = scanner.nextInt();
			
			// Prints a message if the user entered an invalid input. 
			if (days < 1) {
				System.out.println("Invalid input. The number of days must be greater than or equal to 1. \n");
			}			
		}
		
		
		// Instantiates a variable named "total_organisms"
		// This will be used in the folloing for loop. 
		double total_organisms = starting_number;
		
		// Instantiates a string named output with the first
		// two lines formatted by default. 
		// This variable will the added on to in the following
		// for loop/ 	
		String output = "Day    Organisms     \r\n" 
					  + "==================== \r\n";
		
		// For loops based on the number of days.
		for (int count = 0; count < days; count++) {
			
			// Converts the day number to a string. 
			String day_number = Integer.toString(count + 1); 
			
			// Rounds the total organisms to two decimal places, and then
			// converts that value to a string. 
			double total_organisms_rounded = Math.round(total_organisms * 100.0) / 100.0;			
			String total_organisms_str = String.valueOf(total_organisms_rounded);
			
			// Assembles a new line with the day numbers and total number of organisms, 
			// and then adds that line to the final output. 
			String add_line = day_number + "      " + total_organisms_str + "\r\n";
			output += add_line;
			
			// Increases the number of total organisms based off of the daily increase rate.
			// This is important for the next loop. 
			total_organisms += total_organisms * daily_increase; 
		}
		// By the end of the for loop, the output will be properly formatted. 
		
		
		// Prints out the final output. 
		System.out.println(); System.out.println(output);
		
		// Creates a BufferedWriter object which will write a text object 
		// with the name "Population.txt". 
        BufferedWriter writer = 
			new BufferedWriter(new FileWriter("Population.txt"));
		
		// Writes the final output to the text file and
		// closes the writer object. 
		writer.write(output);
        writer.close();
	}
}

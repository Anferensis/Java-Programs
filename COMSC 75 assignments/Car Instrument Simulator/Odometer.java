
/* Written by Albert Ong
 * 
 * COMSC 75
 * Programming assignment #8
 * 
 * Car Instrument Simulator
 */

class Odometer {
	/* A class to represent the odometer of a car 
	 */
	
	// Instantiates three variables	
	// Note: initial_mileage is used exclusively in decrement mileage. 
	
	private int mileage; 			// Used to represent the mileage
	private int initial_miles;		// Used to represent the intitial number of miles
	private FuelGuage fuel_guage;	// A blank FuelGuage object
	
	
	// The default constructor for the Odometer class. 
	public Odometer(int mileage, FuelGuage fuel_guage) {
		
		this.mileage = mileage;
		this.initial_miles = mileage;
		this.fuel_guage = fuel_guage;	
	}
	
	// The no argument constructor for the Odometer class. 
	public Odometer() {
		
		mileage = 0;
		initial_miles = 0;
		fuel_guage = new FuelGuage();
	}
	
	
	// Setter methods for the Odometer class. 
	public void setMileage(int mileage) {
		this.mileage = mileage;
	}
	public void setFuelGuage(FuelGuage fuel_guage) {
		this.fuel_guage = fuel_guage;
	}	
	
	// Getter methods for the Odometer class
	public int getMileage() {
		return mileage;
	}
	public FuelGuage getFuelGuage() {
		return fuel_guage;
	}
	
	
	public void incrementMileage() {
		/* A method that increments the number of miles. 
		 */
		
		mileage++; // Increments the mileage. 
		
		// Calculates the number of miles travel by subtracting the initial
		// mileage from the current mileage. 
		int miles_traveled = mileage - initial_miles;
		
		// The amount of fuel must be decremented if the car has
		// traveled an additional 24 miles. 
		boolean must_decrement_fuel = miles_traveled % 24 == 0;
		
		// Decrements the amount of fuel by one. 
		if (must_decrement_fuel) {
			fuel_guage.decrementFuel();
		}
		
		// Resets the mileage to zero if the mileage exceeds 99,999. 
		if (mileage > 99999) {
			mileage = 0;
		}
	}
	
	// A method that decrements the mileage. 
	public void decrementMileage() {
		this.mileage--;
	}
}


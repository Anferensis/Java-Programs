
/* Written by Albert Ong
 * 
 * COMSC 75
 * Programming assignment #8
 * 
 * Car Instrument Simulator
 */

public class CarInstrumentSimulator {
	/* The driver program of the assignment. 
	 */
	
	public static void main(String args[]) {
		
		// Creates a default Odometer object.
		// The mileage and number of gallons will be zero.
		Odometer odometer = new Odometer();
		
		// Uses a for loop to fill the FuelGuage up to 15 gallons. 
		for (int count = 0; count < 15; count++) {
			odometer.getFuelGuage().incrementFuel();
			
			// Prints out the fuel level for the user. 
			int fuel_level = odometer.getFuelGuage().getGallons();			
			System.out.println("Fuel increased to " + fuel_level + " gallons.");			
		}
		
		// Prints out a blank line
		// The is exclusively for formatting purposes. 
		System.out.println();
		
		// Uses a for loop to simulate running the car for 360 miles. 
		for (int count = 0; count < 361; count++) {
			
			// Prints out the mileage and fuel level for every mile traveled.
			System.out.println("Mileage: " + odometer.getMileage());
			System.out.println("Fuel level: " + odometer.getFuelGuage().getGallons() + " gallons.");
			System.out.println("-----------------------------");
			
			// Increments the mileage. 
			odometer.incrementMileage();
		}
		// By the end of this for loop, the car would have run 360 miles
		// and would have run out of fuel. 
	}
}


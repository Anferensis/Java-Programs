
/* Written by Albert Ong
 * 
 * COMSC 75
 * Programming assignment #8
 * 
 * Car Instrument Simulator
 */

class FuelGuage {
	/* A class to represent the fuel guage of a car. 
	 */
	 
	// A variable to represent the number of gallons. 
	private int gallons;
	
	// The default constructor for the FuelGuage class. 
	public FuelGuage(int gallons) {
		this.gallons = gallons;
	}	
	// The no argument constructor for the FuelGuage class.
	public FuelGuage() {
		gallons = 0;
	}
	
	// Setter method for the FuelGuage class.
	public void setGallons(int gallons) {
		this.gallons = gallons;
	}	
	// Getter method for the FuelGuage class.
	public int getGallons() {
		return this.gallons;
	}
	
	// A method that increments the amount of gallons by one. 
	public void incrementFuel() {
		
		// If the number of gallons is less than 15...
		if (gallons < 15){
			
			// Increments the number of gallons by one. 
			gallons++;
		}
		// Otherwise...
		else {
			
			// Returns an error message. 
			String error_message = "Cannot increase the number of gallons. " + 
								   "The fuel tank is full. ";			
			throw new java.lang.IllegalStateException(error_message);
		}
	}
	
	// A method that decrements the amount of fuel by one. 
	public void decrementFuel() {
		
		// If the number of gallons is greater than 0...
		if (gallons > 0) {
			
			// Decrements the number of gallons by one. 
			gallons--;
		}
		
		// Otherwise...
		else {
			
			// Returns an error message. 
			String error_message = "Cannot decrease the number of gallons. " + 
								   "The fuel tank is empty. ";			
			throw new java.lang.IllegalStateException(error_message);
		}
	}
}
